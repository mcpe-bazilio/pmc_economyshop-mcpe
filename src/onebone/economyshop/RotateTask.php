<?php

namespace onebone\economyshop;

use onebone\economyshop\EconomyShop;
use pocketmine\scheduler\PluginTask;
use pocketmine\network\protocol\MovePlayerPacket;
use pocketmine\level\Position;
use pocketmine\Player;

class RotateTask extends PluginTask {

	/** @var EconomyShop $plugin */
	private $plugin;
	/** @var Player $player */
	private $player;
	/** @var Position $pos */
	private $pos;
	private $yaw;
	private $pitch;

	public function __construct($Plugin, $player, $pos, $yaw, $pitch){
		parent::__construct($Plugin);
		$this->plugin = $Plugin;
		$this->player = $player;
		$this->pos = $pos;
		$this->yaw = $yaw;
		$this->pitch = $pitch;

	}

	public function onRun($tick){

		//$this->player->sendPosition($this->pos, $this->yaw, $this->pitch, MovePlayerPacket::MODE_ROTATION);
		$this->player->sendPosition($this->pos, $this->yaw, $this->pitch, MovePlayerPacket::MODE_RESET);
		//$this->player->sendPosition($this->pos, $this->yaw, $this->pitch, MovePlayerPacket::MODE_NORMAL);
		//$this->player->addMovement($this->pos->getX(), $this->pos->getY(), $this->pos->getZ(), $this->yaw, $this->pitch, $this->yaw);

		//$sender->sendPosition($posToTele, $yaw, $pitch, MovePlayerPacket::MODE_NORMAL);
		//$sender->sendPosition($posToTele, $yaw, $pitch, MovePlayerPacket::MODE_ROTATION);
		return;
	}
}
